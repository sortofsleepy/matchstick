# Matchstick #

A library of sorts for working directly with WebGL. 

Also includes a branch of various helpers for using [Karsten Schmidt](http://postspectacular.com/)
fantastic [thi.ng](http://thi.ng/) libraries that primarily focuses on webgl(via [thi.ng/geom](http://thi.ng/geom) ) but attempts to be agnostic enough to work with OpenGL as well. 


### How do I get set up? ###

* `npm install` to download watchify, etc. If using cljs, theres no `project.clj`, just drag the files into the project you're working on 

* `npm start` to start up watchify and the compilation process
*  run `python -m SimpleHTTPServer <optional port number>` from the public directory to see stuff in the browser.