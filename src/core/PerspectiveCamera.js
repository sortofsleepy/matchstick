import Matrix4 from "../math/Matrix"

class PerspectiveCamera{
    constructor(fov=45,aspect=window.innerWidth/window.innerHeight,near=0.1,far=1000){
        let self = this;
        this._matrix = new Matrix4();
        this._matrix.makePerspective(fov,aspect,near,far);
        this._matrix.translate([0.0,0.0,-1000.0]);

        this.translateVector = [0.0,0.0,-1000.0];
        this.fov = fov;
        this.aspect = aspect;
        this.near = near;
        this.far = far;

        /**
         * Make sure camera matrix updates itself on window resize
         */
        window.addEventListener('resize',function(){

            //TODO how do we account for non full-screen stuff?
            self.aspect = window.innerWidth/window.innerHeight;
            self._matrix.makePerspective(self.fov,self.aspect,self.near,self.far);
            self._matrix.translate(self.translateVector);
        })
    }

    /**
     * Sets a new field of View
     * @param fov new fov value
     * @returns {PerspectiveCamera}
     */
    setFov(fov=60.0){
        this.fov = fov;
        return this;
    }

    /**
     * sets up a default perspective matrix like openFrameworks
     * @param fov field of view
     * @param windowWidth the width of our viewport
     * @param windowHeight the height of our viewport
     */
    setupPerspective(fov,windowWidth,windowHeight){
        fov = fov !== undefined ? fov : 60;
        let eyeX = windowWidth / 2;
        let eyeY = windowHeight / 2;
        //float eyeX = ( 1080*0.5 ) / 2;
        //float eyeY = ( 1920*0.5 ) / 2;
        let halfFov = 3.14149 * fov / 360;
        let theTan = Math.tan(halfFov);
        let dist = eyeY / theTan;
        let nearDist = dist / 10.0;
        let farDist = dist * 10.0;
        this._matrix.makePerspective(fov,windowWidth/windowHeight,nearDist,farDist);
    }

    /**
     * Sets the position of the camera
     * @param vec the new vector to translate to
     * @returns {PerspectiveCamera}
     */
    setPosition(vec){
        vec = vec !== undefined ? vec : [0,0,-400];
        this._matrix.translate(vec);
        this.translateVector = vec;
        return this;
    }

    /**
     * Returns the raw matrix for the camera
     * @returns {*}
     */
    getMatrix(){
        return this._matrix.getRawMatrix();
    }
}

export default PerspectiveCamera;