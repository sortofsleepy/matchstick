const rsvp = require('rsvp');

class Texture {
    /**
     * Constructor
     * @param gl WebGLRenderingContext
     * @param additionalOptions any additional options you might immediately want like min/mag filter setting
     * @param img an optional image path to begin immediately loading a texture
     */
    constructor(gl,additionalOptions={},img){
        if(img !== null && additionalOptions !== null){
            if(additionalOptions.hasOwnProperty('loadCallback')){
                this._loadImage(img,additionalOptions.loadCallback);
            }
        }

        this.gl = gl;

        //default options
        var options = {
            format:"RGBA",
            flipY:true, // texture flipped by default
            minFilter:"NEAREST",
            magFilter:"NEAREST",
            wrapS:"CLAMP_TO_EDGE",
            wrapT:"CLAMP_TO_EDGE",
            usage:"TEXTURE_2D"
        };

        options = this._applyOptions(options,additionalOptions);

        this.wrapS = gl[options.wrapS];
        this.wrapT = gl[options.wrapT];
        this.minFilter = gl[options.minFilter];
        this.magFilter = gl[options.magFilter];
        this.textureFormat = gl[options.format];
        this.textureUsage = gl[options.usage];
        this.flipY = true;

        this.handle = this.texture = gl.createTexture();

        this.textureSet = false;

    }
    _applyOptions(options,additionalOptions){
        for(var i in options){
            if(additionalOptions.hasOwnProperty(i)){
                options[i] = additionalOptions[i];
            }
        }
        return options;
    }
    /**
     * Binds the texture to the specified unit
     */
    bind(index=0,shader){
        let gl = this.gl;

        // we only allow use of this particular function only after we've
        // actually buffered data into it.
        if(this.textureSet){
            this.gl.bindTexture(this.textureUsage,this.handle);

            //activate texture unit
            gl.activeTexture(gl[`TEXTURE${index}`]);
            //gl.uniform1i(gl.getUniformLocation(shader.program,this.textureName),index);
        }

    }

    _applyTextureOptions(){
        let gl = this.gl;
        //flip y if necessary
        if(this.flipY){
            gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
        }

        //apply min/mag filters
        gl.texParameteri(this.textureUsage,gl.TEXTURE_MAG_FILTER,this.magFilter);
        gl.texParameteri(this.textureUsage,gl.TEXTURE_MIN_FILTER,this.minFilter);

        //apply wrapping
        gl.texParameteri(this.textureUsage,gl.TEXTURE_WRAP_S,this.wrapS);
        gl.texParameteri(this.textureUsage,gl.TEXTURE_WRAP_T,this.wrapT);
    }

    /**
     * Start loading a texture.
     * @param url a url or a dom ID to use for the texture
     * @param callback and optional callback to handle loading completion
     */
    loadTexture(url,callback){
        this._loadImage(url,callback);
        return this;
    }

    /**
     * buffers image onto a texture
     * @param image
     * @private
     */
    _bufferTexture(image){
        let gl = this.gl;
        let texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, texture);
        this._applyTextureOptions();
       // gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

        gl.bindTexture(gl.TEXTURE_2D, null);
        this.handle = texture;
        this.textureSet = true;
    }

    unbind(){
        this.gl.bindTexture(this.gl[this.textureType],null);
    }

    /**
     * Processes the img param and loads an image if necessary.
     * When loading an image, a property called "texturePromise" gets created
     * which can be used to decide what to do with the loaded image.
     * @param img
     * @param callback an optional callback to use when loading images
     * @private
     */
    _loadImage(img,callback=null){
        let self = this;
        let gl = this;
        if(img instanceof Image){

        }else if(typeof img === "string"){

            //for simplicity, we only care about ID fetches. If it's not an id,
            //than we assume it's a image path so we load
            if(img.search("#") !== -1){
                this.img = document.querySelector(img);
            }else{
                this.img = new Image();
                this.img.src = img;
                this.img.onload = function(){
                    self._bufferTexture(this);
                };

            }
        }


    }
}

export default Texture;