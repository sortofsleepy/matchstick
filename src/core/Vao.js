/**
 * A basic Vertex Array Object.
 * An attempt was made to keep things simple, but flexible.
 *
 * Will use native Vertex Array Objects if that extension is available. Otherwise will fallback to an "emulated" method
 * of binding and unbinding various buffers.
 */
class Vao {
    /**
     * Constructor. Initializes our Vertex Array Object
     * @param gl a WebGLRenderingContext object
     * @param attribs an optional array of attributes following the specified convention. See documentation for what that is
     * @param shader a WebGLShader object so we can obtain attribute locations
     */
    constructor(gl,attribs=null,shader=null,indexBuffer=null){
        this.gl = gl;
        this.attributes = attribs !== undefined ? attribs : [];
        this.primitive = gl.TRIANGLES;
        this._hasIndexBuffer = false;
        this.numVertices = 4;

        //check for OES_vertex_array extension
        var ext = gl.getExtension("OES_vertex_array_object");
        if(ext !== null){
            this.vao = ext.createVertexArrayOES();
            this.ext = ext;
            this.USE_VAO = true;
        }

        //if we've passed in a set of attributes
        if(attribs !== null && shader !== null){
            this.shader = shader;
            if(this.USE_VAO){
                this.bind();
                this._nativeBufferAllData(attribs,shader);
                this.unbind();
            }else{
                this._emulatedBufferData(attribs,shader);
            }
        }

        /**
         * if we've passed in Indices
         */
        if(indexBuffer !== null){
            this.setIndexBuffer(indexBuffer);
        }
    }

    /**
     * Sets up indices for drawing.
     * @param indexBuffer
     */
    setIndexBuffer(indexBuffer,type="UNSIGNED_SHORT",offset=0){
        this._indexBufferType = this.gl[type];
        this._hasIndexBuffer = true;
        this._indexBuffer = indexBuffer;
        this._indexBufferOffest = offset;
        this.numVertices = indexBuffer.data.length;
    }

    /**
     * Returns whether or not we can use the native vao object
     * @returns {boolean}
     */
    canUseVao(){
        return this.USE_VAO;
    }

    /**
     * Draws the object's attributes. You can also simultaneously update the
     * attribute data if need be.
     * @param attribs an array of attributes to update following the specified convention
     * @param shader if you haven't set a shader yet by now, you can also pass in a shader so the object can fetch the correct attribute location for the attribute that needs to be drawn/set
     */
    draw(attribs,shader){
        //if we have't defined a shader till this point, save it
        if(this.shader === undefined){
            this.shader = shader !== undefined ? shader : false;
        }

        /**
         * If we need to , update data.
         */
        if(Array.isArray(attribs)){
            this.update(attribs);
        }

        if(this.USE_VAO){
            this._drawBuffers();
        }else if(!this.USE_VAO){
            this._emulatedDrawBuffers();
        }
    }

    /**
     * Updates the vao data(or buffer data if your graphics card doesn't support
     * native Vertex Array Objects.)
     * @param attribs
     */
    update(attribs){
        if(this.USE_VAO){
            this.bind();
            this._nativeBufferAllData(attribs);
            this.unbind();
        }else{
            this._emulatedBufferData(attribs);
        }
    }

    /**
     * Sets the number of vertices to be drawn.
     * @param numVertices
     */
    setNumberOfVertices(numVertices=4){
        this.numVertices = numVertices;
    }

    /**
     * Sets the primitive type to be used when drawing buffers.
     * Used when rendering attributes via a native Vao object.
     * @param type
     */
    setPrimativeType(type="TRIANGLES"){
        this.primitive = this.gl[type];
        return this;
    }

    /**
     * Draws all the attribute on the vao
     * @param numVertices
     * @private
     */
    _drawBuffers(numVertices){
        let gl = this.gl;
        let usageType = this.primitive;
        numVertices = numVertices !== undefined ? numVertices : this.numVertices;
        this.bind();
        if(this._hasIndexBuffer){
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,this._indexBuffer.handle);
            gl.drawElements(usageType,this.numVertices,this._indexBufferType,this._indexBufferOffest);
        }else{
            gl.drawArrays(usageType,0,numVertices);
        }
        this.unbind();
    }
    /**
     * Emulated attribute drawing. For each "attribute", we bind and draw the data.
     * In order to which bits need to be drawn and which simply need to be pointed, to,
     * make sure to specify a primitive type with the attribute object you set up.
     * @private
     */
    _emulatedDrawBuffers(){
        let attributes = this.attributes;
        let len = attributes.length;
        let gl = this.gl;
        let shader = this.shader;

        for(let i = 0; i < len; ++i){
            let attribute = attributes[i];
            let numVertices = attribute.numVertices !== undefined ? attribute.numVertices : this.numVertices;
            let primitive = attribute.primitive !== undefined ? attribute.primitive : false;
            let size = attribute.size !== undefined ? attribute.size : 3;
            let type = attribute.type !== undefined ? attribute.type : gl.FLOAT;
            let normalized = attribute.normalized !== undefined ? attribute.normalized : false;
            let stride = attribute.stride !== undefined ? attribute.stride : 0;
            let offset = attribute.offset !== undefined ? attribute.offset : 0;
            let name = attribute.name !== undefined ? attribute.name : "";

            attribute.buffer.bind();

            //if a primitive value is specified, this indicates that we need to draw that attribute
            if(primitive){
                if(this._hasIndexBuffer){
                    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,this._indexBuffer.handle);
                    gl.drawElements(this.primitive,this._indexBuffer.data.length,this._indexBufferType,attribute.buffer.indexBufferOffset );
                }else{

                    gl.drawArrays(gl[primitive],0,numVertices);
                }

            }else{
                //otherwise just point to that attribute
                let attribLocation = shader[name].location;
                gl.vertexAttribPointer(attribLocation,size,type,normalized,stride,offset);
            }
        }

    }

    /**
     * Used when we don't have native VAO objects.
     * Used to set the vertexAttrib pointer for the intial set and all subsequent updates
     * @param attributes
     * @param shader
     * @private
     */
    _emulatedBufferData(attributes=null,shader){
        let len = attributes.length;
        shader = shader !== undefined ? shader : this.shader;
        let gl = this.gl;


        for(let i = 0; i < len; ++i){
            let attribute = attributes[i];
            let size = attribute.size !== undefined ? attribute.size : 3;
            let type = attribute.type !== undefined ? attribute.type : gl.FLOAT;
            let normalized = attribute.normalized !== undefined ? attribute.normalized : false;
            let stride = attribute.stride !== undefined ? attribute.stride : 0;
            let offset = attribute.offset !== undefined ? attribute.offset : 0;
            let name = attribute.name !== undefined ? attribute.name : "";


            attribute.buffer.bind();
            let attribLocation = shader[name].location;
            gl.vertexAttribPointer(attribLocation,size,type,normalized,stride,offset);
            attribute.buffer.unbind();
        }

    }

    /**
     * For use when we have native Vao objects
     * @param attribs
     */
    _nativeBufferAllData(attributes,shader){
        let len = attributes.length;
        shader = shader !== undefined ? shader : this.shader;
        let gl = this.gl;

        //bind vao
        this.bind();

        //loop through and bind all attributes on the vao
        for(let i = 0; i < len; ++i){
            let attribute = attributes[i];
            let size = attribute.size !== undefined ? attribute.size : 3;
            let numVertices = attribute.numVertices !== undefined ? attribute.numVertices : 3;
            let type = attribute.type !== undefined ? attribute.type : gl.FLOAT;
            let normalized = attribute.normalized !== undefined ? attribute.normalized : false;
            let stride = attribute.stride !== undefined ? attribute.stride : 0;
            let offset = attribute.offset !== undefined ? attribute.offset : 0;
            let name = attribute.name !== undefined ? attribute.name : "";

            //cache number of vertices to draw
            this.numVertices = numVertices;

            gl.enableVertexAttribArray(shader[name].location);
            attribute.buffer.bind();
            let attribLocation = shader[name].location;
            gl.vertexAttribPointer(attribLocation,size,type,normalized,stride,offset);
            attribute.buffer.unbind();
        }

        //unbind vao
        this.unbind();
    }

    bind(){
        if(this.USE_VAO){
            this.ext.bindVertexArrayOES(this.vao);
        }
    }

    unbind(){
        if(this.USE_VAO){
            this.ext.bindVertexArrayOES(null);
        }
    }
}



export default Vao;