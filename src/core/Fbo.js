
class Fbo {
    constructor(gl,data,options){
        this.gl = gl;
        this.gl = gl;
        this.handle = gl.createFramebuffer();

        this.attachments = [];

        // stores references to all of the textures
        this.textures = [];

        this.mipmaps = false;

        //references the main(or sometimes the ONLY) texture which is always at COLOR_ATTACHMENT_0
        this.texture = null;

        //setup options
        this._setupOptions(options);

        // prep extension for checking for floating point textures (fpt = floating point textures)
        this.fpt = gl.getExtension("OES_texture_float");

        //use extension based attachments if at all possible if we have webgl1
        if(!gl.checkWebgl2()){
            var t_ext = gl.getExtension('WEBGL_draw_buffers');
            if(t_ext){
                this.buffer_ext = t_ext;
                this.canUseDrawbuffers = true;
                this.drawCommand = t_ext['drawBuffersWEBGL'];
                this.maxAttachments = t_ext.MAX_COLOR_ATTACHMENTS_WEBGL;
                this._initColorAttachments(t_ext);
            }else{
                this.maxAttachments = 1;
            }
        }

        // if the card supports it, use floating point textures by default
        this.t_float = gl.getExtension("OES_texture_float");

        //initialize
        this._initialize(data);
    }

    /**
     * Binds frame buffer for rendering
     */
    bind(){
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.handle);
    }

    /**
     * Unbinds frame buffer for rendering
     */
    unbind(){
        let gl = this.gl;
        if(this.mipmaps){
            let currentTexture =  gl.getParameter(gl.ACTIVE_TEXTURE) - gl.TEXTURE0;
            gl.bindTexture(gl.TEXTURE_2D, currentTexture);
            gl.generateMipmap(gl.TEXTURE_2D);
            gl.bindTexture(gl.TEXTURE_2D, null);
        }
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
    }

    bindTexture(index=0){
        let gl = this.gl;

        // activate texture - equivalent of gl::ScopedTexture in Cinder
        gl.activeTexture(gl[`TEXTURE${index}`]);

        //bind the texture
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
        this.activeTexture = index;
    }


    unbindTexture(){
        this.gl.bindTexture(this.gl.TEXTURE_2D, null);
    }

    getTexture(){
        return this.gl.getParameter(this.gl.ACTIVE_TEXTURE) - this.gl.TEXTURE0;
    }

    /**
     * Adds another texture layer to the FBO on a secondary attachment
     * @param data
     */
    addLayer(data=null,options){
        // initialize a new texture
        this._initialize(data,this.textures.length + 1,options);
    }

    /**
     * Initialize fbo with a texture.
     * @param data any data we want to intialize onto the texture that gets creates
     * @param attachment the attachment number we want to use
     * @param options any options that you'd like to use to override the initialiily set / default options
     * @private
     */
    _initialize(data=null,attachment=0,options){
        let gl = this.gl;
        let width = this.options.width;
        let height = this.options.height;

        // make sure height and width are within the capabilities of the card
        var maxFBOSize = gl.getParameter(gl.MAX_RENDERBUFFER_SIZE)
        if(width < 0 || width > maxFBOSize || height < 0 || height > maxFBOSize) {
            throw new Error('gl-fbo: Parameters are too large for FBO')
        }

        //ensure the attachment value doesn't exceed the max supported
        if(attachment > this.maxAttachments){
            throw new Error("Bad attachment point specified in Fbo#_initialize : exceeds max number of attachment points");
        }

        /**
         * If we're initializing with a Float32 array and we're able to support
         * floating point textures, switch texture option to gl.FLOAT
         */
        if(data instanceof Float32Array){
            if(this.fpt !== null) {
                this.options.type = gl.FLOAT;
            }
        }

        //bind fbo
        this.bind();

        //create texture
        var texture = gl.createTexture();

        //bind texture for rendering
        gl.bindTexture(gl.TEXTURE_2D, texture);

        //apply settings for how the texture should act
        this._applyTextureSettings();

        //setup texture data. will be null(or all 0s) if data is null
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width,height, 0, gl.RGBA, this.options.type, data);

        //link texture onto the framebuffer
        gl.framebufferTexture2D(gl.FRAMEBUFFER, this.attachments[attachment], gl.TEXTURE_2D, texture, 0);

        //unbind the texture
        gl.bindTexture(gl.TEXTURE_2D, null);


        this._checkFramebufferStatus();

        //unbind fbo
        this.unbind();


        // if texture attribute is null, we know this is the default layer, so append.
        if(this.texture === null){
            this.texture = texture;

            //just push a number in so we can properly keep track of how many attachments are used
            this.textures.push(0);
        }else{
            //otherwise start pushing textures into the attachments array
            this.textures.push(texture);
        }
    }

    /**
     * Checks the status of the FBO to ensure things are ok. Will throw
     * error if not.
     * @private
     */
    _checkFramebufferStatus(){
        let gl = this.gl;
        let status = gl.checkFramebufferStatus(gl.FRAMEBUFFER);

        if(status !== gl.FRAMEBUFFER_COMPLETE){
            switch(status){
                case gl.FRAMEBUFFER_UNSUPPORTED:
                    throw new Error('gl-fbo: Framebuffer unsupported')
                case gl.FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                    throw new Error('gl-fbo: Framebuffer incomplete attachment')
                case gl.FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
                    throw new Error('gl-fbo: Framebuffer incomplete dimensions')
                case gl.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                    throw new Error('gl-fbo: Framebuffer incomplete missing attachment')
                default:
                    throw new Error('gl-fbo: Framebuffer failed for unspecified reason')
            }
        }
    }

    /**
     * Sets up options we use for the fbo
     * @param options
     * @private
     */
    _setupOptions(options){
        let gl = this.gl;
        //default texture options
        var textureOptions = {
            format:gl["RGBA"],
            flipY:true,
            unpackAlignment:4, // valid values: 1, 2, 4, 8 (see http://www.khronos.org/opengles/sdk/docs/man/xhtml/glPixelStorei.xml)
            minFilter:gl["NEAREST"],
            magFilter:gl["NEAREST"],
            wrapS:gl["CLAMP_TO_EDGE"],
            wrapT:gl["CLAMP_TO_EDGE"],
            width:128,
            height:128,
            type:gl["UNSIGNED_BYTE"],
            internalFormat:gl["RGBA"]
        }

        //! height/width of the fbo
        this.shape = [textureOptions.width,textureOptions.height];

        //override any current settings with settings from the object that was passed in
        for(var i in options){
            if(textureOptions.hasOwnProperty(i)){
                textureOptions[i] = options[i];
            }
        }

        this.options = textureOptions;
    }

    /**
     * Applies current texture settings
     */
    _applyTextureSettings(){
        let gl = this.gl;
        gl.pixelStorei(gl.UNPACK_ALIGNMENT,1);
        if(this.options.flipY){
            gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
        }

        //generate mipmaps
        //gl.generateMipmap(gl.TEXTURE_2D);

        //apply min/mag filters
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,this.options.magFilter);
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,this.options.minFilter);
        //apply wrapping
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,this.options.wrapS);
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,this.options.wrapT);
    }

    /**
     * Stores all available COLOR_ATTACHMENT<number>_WEBGL slots in the
     * attachments array of the Fbo object
     * @param ext the variable containing the extension related code. Will likely only be necessary until the extension is promoted.
     * @private
     */
    _initColorAttachments(ext){
        let gl = this.gl;
        var maxColorAttachments = gl.getParameter(ext.MAX_COLOR_ATTACHMENTS_WEBGL)
        // colorAttachmentArrays = new Array(maxColorAttachments + 1)
        for(var i = 0; i < maxColorAttachments; ++i){
            this.attachments.push(ext[`COLOR_ATTACHMENT${i}_WEBGL`])
        }
    }
}

export default Fbo;