import Matrix4 from "../math/Matrix"
import Vao from "./Vao.js"

class Vbo {
    constructor(gl,data,bufferType="ARRAY_BUFFER",indexBufferType="UNSIGNED_SHORT",indexBufferOffset=0,usage="DYNAMIC_DRAW"){
        this.gl = gl;
        this.mvMatrix = this.modelViewMatrix = new Matrix4();
        this.buffer = this.handle = gl.createBuffer();

        //type of buffer the item is - either gl.ARRAY_BUFFER or gl.ELEMENT_ARRAY_BUFFER
        this.type = gl[bufferType];

        if(this.type === gl.ELEMENT_ARRAY_BUFFER){
            this.indexBufferOffset = indexBufferOffset;
            //if this vbo is acting as a indices buffer, this is used as the data type
            this.indexBufferType = indexBufferType
        }

        //usage - gl.STATIC_DRAW, gl.DYNAMIC_DRAW etc
        this.usage = gl[usage];

        //data to put in the buffer
        this.data = data;

        //set default translation for modelViewMatrix
        //TODO this should be moved out
        this.mvMatrix.translate([0,0,-5.0]);

        //if we've passed in array data, buffer that right away
        if(this.data !== undefined){
            this.bufferData();
        }
    }

    /**
     * Sets an offset value to use when we have an ELEMENT_ARRAY_BUFFER.
     * By it is 0
     * @param offset
     */
    setIndexBufferOffset(offset){
        this.indexBufferOffset = offset;
    }

    /**
     * Updates the usage type
     * @param usageType can be "STATIC_DRAW" or "DYNAMIC_DRAW"
     * TODO remember to find and mention the other usage types
     */
    setUsage(usageType=null){
        if(usageType !== null){
            this.usage = this.gl[usageType];
        }

        return this;
    }

    /**
     * Updates the default buffer type.
     * @param bufferType can be "ARRAY_BUFFER" or "ELEMENT_ARRAY_BUFFER"
     */
    setBufferType(bufferType=null){
        if(bufferType !== null){
            this.type = this.gl[bufferType];
        }

        return this;
    }

    bufferData(){
        let gl = this.gl;
        this.bind();
        gl.bufferData(this.type,this.data,this.usage);
        this.unbind();
    }

    bind(){
        let gl = this.gl;
        gl.bindBuffer(this.type,this.handle);
    }

    unbind(){
        let gl = this.gl;
        gl.bindBuffer(this.type,null);
    }
}

export default Vbo;