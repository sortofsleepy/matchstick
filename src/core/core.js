/**
 * Ensures that the value is a power of 2
 * @param value the value to check
 * @returns {boolean} returns true or false depending on if the value is a power of 2
 */
export function isPowerOf2(value){
    return (value & (value - 1)) == 0;
}