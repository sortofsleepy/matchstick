import Texture from "./Texture"

class WebGLProgram {

    /**
     * Constuctor. Will not initialize without GLSL source.
     * @param context a WebGLRenderingContext
     * @param vertexSource the source for the vertex shader
     * @param fragmentSource the source for the fragment shader.
     */
    constructor(context=null,vertexSource=null,fragmentSource=null){
        if(vertexSource === null || fragmentSource === null || context === null){
            console.error("WebGLProgram#constructor - can't load program, need shaders")
            return;
        }

        this.gl = context;
        this.boundTextures = [];
        try{
            var vertex = document.querySelector(vertexSource);
            vertexSource = vertex.textContent;
        }catch(e){}

        try {
            var fragment = document.querySelector(fragmentSource);
            fragmentSource = fragment.textContent;
        }catch(e){}

        this.numAttributes = 0;

        this._createProgram(vertexSource,fragmentSource);
        this._applyUniformsAndAttributes();
    }

    /**
     * This automatically applies all of the attributes and uniforms used in the shader
     * as values on-top of the WebGLProgram for easy access.
     * @private
     */
    _applyUniformsAndAttributes(){
        let gl = this.gl;
        let uniforms = gl.getProgramParameter(this.program,gl.ACTIVE_UNIFORMS);
        let attributes = gl.getProgramParameter(this.program,gl.ACTIVE_ATTRIBUTES);

        for(var i = 0; i < uniforms;++i){
            var uniform = gl.getActiveUniform(this.program,i);
            this[uniform.name] = {
                shaderName:uniform.name,
                location:gl.getUniformLocation(this.program,uniform.name)
            }
        }

        for(var i = 0; i < attributes; ++i){
            var attribute = gl.getActiveAttrib(this.program,i);
            this[attribute.name] = {
                shaderName:attribute.name,
                location:gl.getAttribLocation(this.program,attribute.name)
            }

            this.numAttributes += 1;

            //enable attributes
            gl.enableVertexAttribArray(this[attribute.name].location);
        }
    }

    _createProgram(vertex,fragment){
        let gl = this.gl;

        let vShader = this._constructShader(gl.VERTEX_SHADER,vertex);
        let fShader = this._constructShader(gl.FRAGMENT_SHADER,fragment);

        if(vShader !== false && fShader !== false){
            let program = gl.createProgram();
            gl.attachShader(program,vShader);
            gl.attachShader(program,fShader);
            gl.linkProgram(program);

            if(!gl.getProgramParameter(program,gl.LINK_STATUS)){
                console.error("Could not initialize shaders");
                return false;
            }else{

                this.program = this.shader = program;
                return program;
            }
        }
    }

    _constructShader(type,source) {
        let gl = this.gl;
        let shader = gl.createShader(type);

        gl.shaderSource(shader, source);
        gl.compileShader(shader);

        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            console.error(gl.getShaderInfoLog(shader));
            return false;
        } else {
            return shader;
        }

    }

    /**
     * A clearer way to retrieve attributes/uniforms from off of the object
     * @param uniformName the uniform or attribute name to look up
     * @returns {*}
     */
    getUniformOrAttribute(uniformName){
        if(this.hasOwnProperty(uniformName)){
            return this[uniformName];
        }else{
            return false;
        }
    }
    /**
     * Sets the camera for use in the shader
     * @param camera the Camera object to utilize
     * @param projectionMatrixName an alternate name for the projection matrix if necessary
     */
    useCamera(camera,projectionMatrixName="projectionMatrix"){
        let gl = this.gl;
        //find the uniform location on the program
        gl.uniformMatrix4fv(this[projectionMatrixName].location,false,camera.getMatrix());
        return this;
    }

    /**
     * Sets the current model-view matrix to use in the shader
     * @param mvMatrix the Matrix object to use
     * @param mvMatrixName an alternate name for the matrix if necessary
     */
    useModelViewMatrix(mvMatrix,mvMatrixName="modelViewMatrix"){
        let gl = this.gl;
        //find the uniform location on the program
        gl.uniformMatrix4fv(this[mvMatrixName].location,false,mvMatrix.getRawMatrix());
    }

    /**
     * Binds the shader for use. Note all parameters are optional
     * @param camera the camera to use for the shader
     * @param projectionMatrixName an alternate name for the projectionMatrix if need be.
     */
    bind(options={}){
        /**
         * We only call use program if there is no currently bound shader.
         * You need to call unbind before another shader can be bound.
         * Keep track on top of window variable "SHADER_BOUND" to minimize confusion and having to pass
         * shader programs back and forth between things.
         */
        if(window.SHADER_BOUND === undefined || !window.SHADER_BOUND){
            this.gl.useProgram(this.program);
            this.isBound = true;
            window.SHADER_BOUND = this;
        }else{
            console.error("Unable to bind shader - shader is already bound");
        }

        if(options.hasOwnProperty('camera')){
            let name = options.projectionMatrixName !== undefined ? options.projectionMatrixName : "projectionMatrix";
            this.useCamera(options.camera,name);
        }

        // set and prepare any uniforms
        if(options.hasOwnProperty('uniforms')){
            for(var i in options["uniforms"]){
                let uniform = options["uniforms"][i];
                if(uniform instanceof WebGLTexture){
                    this._applyTexture(uniform,i);
                }
            }
        }


    }

    /**
     * Unbinds the shader
     */
    unbind(){
        if(window.SHADER_BOUND !== undefined){
            this.isBound = false;
            this.gl.useProgram(null);
            window.SHADER_BOUND = undefined;

        }
    }

    //-------------- UNIFORM HANDLING ---------------------
    /**
     * Applies texture to the program
     * @param unit the texture unit to bind to
     * @param name the uniform name taken from the Object passed to the WebGLShader#bind function
     * @private
     */
    _applyTexture(unit,name){
        let gl = this.gl;
        gl.uniform1i(gl.getUniformLocation(this.shader,name),uniform);
    }
}

export default WebGLProgram;