const raf = require('raf');
class WebGLRenderer {
    constructor(options={width:window.innerWidth,height:window.innerHeight}){
        this._buildContext();

        this.width = options.width;
        this.height = options.height;
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.viewportX = 0;
        this.viewportY = 0;
        this.clearColor = [0,0,0,1];



        this._applyProps();

        return this;
    }

    getContext(){
        return this.gl;
    }
    _applyProps(){
        for(var i in this.gl){
            this[i] = this.gl[i];
        }
    }
    _buildContext(){
        let types = [
            "webgl2",
            "experimental-webgl2",
            "webgl",
            "experimental-webgl"
        ]

        var canvas = document.createElement("canvas");

        for(var i = 0; i < types.length; ++i){
            let ctx = canvas.getContext(types[i]);
            if(ctx !== null){
                this.contextType = types[i];
                this.canvas = canvas;
                this.gl = ctx;
                this.gl.type = this.contextType;
                this.gl.checkWebgl2 = function(){
                    if(this.type !== "webgl2" || this.type !== "experimental-webgl2"){
                        return false;
                    }
                }
                break;
            }
        }
    }

    /**
     * Makes the WebGL canvas fullscreen. Will also apply a resize listener
     * to ensure that the canvas remains fullscreen even if its resized
     * @param customResizeCallback a optional value that will run instead of the default function
     * @returns {WebGLRenderer}
     */
    setFullscreen(customResizeCallback=null){
        let self = this;
        let gl = this.gl;
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;

        window.addEventListener("resize",function(){
            if(customResizeCallback !== null){
                customResizeCallback(self);
            }else{
                self.canvas.width = window.innerWidth;
                self.canvas.height = window.innerHeight;
                gl.viewport(self.viewportX,self.viewportY,self.canvas.width,self.canvas.height);

                if(self.hasOwnProperty("camera")){
                    //TODO adjust camera as necessary
                }
                //make sure viewport is reset
                self.gl.viewport(self.viewportX,self.viewportY,self.canvas.width,self.canvas.height);
            }


        });

        this.gl.viewport(self.viewportX,self.viewportY,self.canvas.width,self.canvas.height);
        return this;
    }

    /**
     * Appends the canvas to the DOM.
     * @param el the element you want to append to. By default will append to body
     */
    appendTo(el=document.body){
        el.appendChild(this.canvas);
    }

    /**
     * Clears the background color
     */
    clearScreen(){
        let gl = this.gl;
        let color = this.clearColor;
        gl.clearColor(color[0],color[1],color[2],color[3]);
        gl.viewport(this.viewportX,this.viewportY, this.canvas.width,this.canvas.height);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        gl.enable(gl.DEPTH_TEST);
    }

    /**
     * Renders the scene
     * @param scene
     * @param camera
     */
    render(scene,camera){
        let gl = this.gl;
        this.clearScreen();
    }


    ////////////// STATIC FUNCTIONS /////////////////

    /**
     * Sets the clear color for the context
     * @param r the red value to set
     * @param g the green value to set
     * @param b the blue value to set
     * @param a the alpha channel value to set
     */
    static setClearColor(instance,r=0,g=0,b=0,a=1){
        instance.clearColor[0] = r;
        instance.clearColor[1] = g;
        instance.clearColor[2] = b;
        instance.clearColor[3] = a;
    }

}

export default WebGLRenderer;
