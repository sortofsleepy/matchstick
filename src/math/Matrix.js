const mat4 = require('gl-mat4');

class Matrix4 {
    constructor(matrix=null){
        if(matrix === null){
            this.mat = mat4.create();
        }else{
            this.mat = matrix;
        }
    }

    translate(vector){
        this.mat = mat4.translate(this.mat,this.mat,vector);
        return this;
    }

    /**
     * Alias around the identity function
     * @returns {Matrix4}
     */
    reset(){
        return this.identity();
    }

    /**
     * Wrapper around setting up an identity matrix
     * @returns {Matrix4}
     */
    identity(){
        this.mat = mat4.identity(this.mat);
        return this;
    }

    getRawMatrix(){
        return this.mat;
    }

    /**
     * Wrapper around gl-matrix's perspective function
     * @param fov field of view
     * @param aspect aspect ratio
     * @param near nearclip value
     * @param far farclip value
     * @returns {Matrix4} returns object for chaining
     */
    makePerspective(fov=45,aspect=window.innerWidth/window.innerHeight,near=0.1,far=1000){
        this.mat = mat4.perspective(this.mat,fov,aspect,near,far);
        return this;
    }
}

export default Matrix4;