import WebGLShader from "../core/WebGLShader"
const glslify = require('glslify');

class BasicMaterial extends WebGLShader {
    constructor(gl,vertexSource = glslify('../shaders/vertex-core.glsl'),fragmentSource = glslify('../shaders/texture-core.glsl')){
        super(gl,vertexSource,fragmentSource);
    }
}

export default BasicMaterial;