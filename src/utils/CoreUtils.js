const uuid = require('node-uuid');

/**
 * Generates a unique identifier that can be used for contexts and other things
 * @param options optional values to pass to the generator
 * @returns {*} uuid
 */
export function GenerateId(options={}){
    return uuid.v4(options);
}