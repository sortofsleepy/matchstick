#pragma glslify: attributes = require('./core/attributes',position=position,uv=uv)
#pragma glslify: uniforms = require('./core/uniforms',projectionMatrix=projectionMatrix,modelViewMatrix=modelViewMatrix)

varying vec2 vUv;
void main(void) {
    vUv = uv;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);
}