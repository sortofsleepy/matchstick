precision highp float;

// demonstrates basic use of a texture

uniform sampler2D uTex0;
varying vec2 vUv;
void main() {
    vec4 image = texture2D(uTex0,vUv);
    gl_FragColor = image;
}
