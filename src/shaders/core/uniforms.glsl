//core uniforms that are universal for just about every shader
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

#pragma glslify: export(modelViewMatrix,projectionMatrix);