// borrowed from @toxi's excellent thi.ng libs
//https://github.com/thi-ng/shadergraph/blob/master/src/fog.org

float fogLinear(float dist, float start, float end){
    return 1.0 - clamp((end - dist) / (end - start), 0.0,1.0);
}

float fogExp(float dist, float density) {
  return 1.0 - clamp(exp(-density * dist), 0.0, 1.0);
}

float LOG2 = -1.442695;

float fogExp2(float dist, float density) {
  float d = density * dist;
  return 1.0 - clamp(exp2(d * d * LOG2), 0.0, 1.0);
}

#pragma glslify: export(fogLinear,fogExp,fogExp2);