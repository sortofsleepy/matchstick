//core attributes that are pretty much universal in every shader.
attribute vec3 position;
attribute vec2 uv;
attribute vec3 normal;


#pragma glslify: export(position,uv,normal);