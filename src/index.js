import WebGLRenderer from "./core/WebGLRenderer"
import WebGLShader from "./core/WebGLShader"
import PerspectiveCamera from "./core/PerspectiveCamera"
import Plane from "./primatives/Plane"
import BasicMaterial from "./Materials/BasicMaterial"
import Texture from "./core/Texture"
import raf from 'raf'
const glslify = require('glslify');

var renderer = new WebGLRenderer().setFullscreen();
renderer.appendTo();

var b = new BasicMaterial(renderer.getContext());

var program = new WebGLShader(renderer.getContext(), glslify('./shaders/vertex-core.glsl'), glslify('./shaders/fragment-core.glsl'));

//build camera
var camera = new PerspectiveCamera(45, window.innerWidth/window.innerHeight, 1.0, 4096.0);
var t = new Texture(renderer.getContext());
t.loadTexture("/img/test.jpg")

//build square vertices
var p = new Plane(renderer.getContext(),b);

raf(function tick(dt){
    renderer.render();
    t.bind();
    b.bind({
        camera:camera,
        uniforms:{
            uTex0:0
        }
    });
    p.draw();
    b.unbind();

    raf(tick);
});
