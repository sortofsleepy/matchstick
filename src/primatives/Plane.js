import Vbo from "../core/Vbo"
import Vao from "../core/Vao"

class Plane {
    constructor(gl,shader){
        this.gl = gl;
        this.numVertices = 4;
        this.shader = shader !== undefined ? shader : false;
        var vertices = new Float32Array(  [
            1.0,  1.0,  0.0,
            -1.0,  1.0,  0.0,
            1.0, -1.0,  0.0,
            -1.0, -1.0,  0.0
        ]);

        this._buildPlane(500,200,2,2);
        this.buffer = new Vbo(gl,this.vertices);
        var indices = new Vbo(gl,this.indices,"ELEMENT_ARRAY_BUFFER");
        var uvs = new Vbo(gl,this.uvs);
        this.vao = new Vao(gl,[
            {
                buffer:this.buffer,
                size:3,
                numVertices:4,
                name:"position",
                primitive:"TRIANGLE_STRIP"
            },{
                buffer:uvs,
                size:2,
                name:"uv"
            }
        ],shader,indices);
    }

    draw() {
        let gl = this.gl;
        window.SHADER_BOUND.useModelViewMatrix(this.buffer.mvMatrix);

        this.vao.draw();
    }

    _buildPlane(width,height,widthSegments,heightSegments){
        this.primative = this.gl.TRIANGLE_STRIP;
        let gl = this.gl;


        var width_half = width / 2;
        var height_half = height / 2;

        var gridX = Math.floor( widthSegments ) || 1;
        var gridY = Math.floor( heightSegments ) || 1;

        var gridX1 = gridX + 1;
        var gridY1 = gridY + 1;

        var segment_width = width / gridX;
        var segment_height = height / gridY;

        var vertices = new Float32Array( gridX1 * gridY1 * 3 );
        var normals = new Float32Array( gridX1 * gridY1 * 3 );
        var uvs = new Float32Array( gridX1 * gridY1 * 2 );

        var offset = 0;
        var offset2 = 0;

        for ( var iy = 0; iy < gridY1; iy ++ ) {

            var y = iy * segment_height - height_half;

            for ( var ix = 0; ix < gridX1; ix ++ ) {

                var x = ix * segment_width - width_half;

                vertices[ offset ] = x;
                vertices[ offset + 1 ] = - y;

                normals[ offset + 2 ] = 1;

                uvs[ offset2 ] = ix / gridX;
                uvs[ offset2 + 1 ] = 1 - ( iy / gridY );

                offset += 3;
                offset2 += 2;

            }

        }

        offset = 0;

        var indices = new ( ( vertices.length / 3 ) > 65535 ? Uint32Array : Uint16Array )( gridX * gridY * 6 );

        for ( var iy = 0; iy < gridY; iy ++ ) {

            for ( var ix = 0; ix < gridX; ix ++ ) {

                var a = ix + gridX1 * iy;
                var b = ix + gridX1 * ( iy + 1 );
                var c = ( ix + 1 ) + gridX1 * ( iy + 1 );
                var d = ( ix + 1 ) + gridX1 * iy;

                indices[ offset ] = a;
                indices[ offset + 1 ] = b;
                indices[ offset + 2 ] = d;

                indices[ offset + 3 ] = b;
                indices[ offset + 4 ] = c;
                indices[ offset + 5 ] = d;

                offset += 6;

            }

        }

        this.vertices = vertices;
        this.indices = indices;
        this.uvs = uvs;

    }
}

export default Plane;